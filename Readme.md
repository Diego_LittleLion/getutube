# GetUTube
Herramienta para descargar tanto vídeo como audio de un vídeo de Youtube.
Esta herramienta se apoya sobre yt-dlp.

## Instrucciones
1. Descargar el fichero GetUTube.bat
2. Ejecutar el archivo. Es posible que salte una advertencia de seguridad. Hacemos click en "Continuar de todas formas".
3. Introducimos la URL del vídeo del que queramos descargar el vídeo o el audio.
4. Elegimos si queremos descargar el audio o el vídeo.
5. Una vez finalizada la descarga tendremos el fichero resultante en la carpeta "Descargas" de nuestro usuario.