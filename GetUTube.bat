@echo off

:: Desarrollado por Diego Littlelion
:: https://gitlab.com/Diego_LittleLion/getutube

:: Variables
set yt-dlp_url="https://github.com/yt-dlp/yt-dlp/releases/download/2023.03.04/yt-dlp.exe"
set yt-dlp_dir="%temp%\yt-dlp.exe"
set ffmpeg_url="https://github.com/GyanD/codexffmpeg/releases/download/7.0/ffmpeg-7.0-essentials_build.zip"
set ffmpeg_dir="%temp%\ffmpeg-7.0-essentials_build\bin\ffmpeg.exe"

:: Comprobamos que los requisitos estan instalados
:: Descarga el archivo yt-dlp.exe si no lo esta ya
if not exist "%yt-dlp_dir%" (
    echo Descargando yt-dlp...
    powershell -Command "(New-Object System.Net.WebClient).DownloadFile('%yt-dlp_url%', '%yt-dlp_dir%')"
    "%yt-dlp_dir%" -U
) else (
    "%yt-dlp_dir%" -U > NUL
)

:: Descarga de ffmpeg si no lo esta ya
if not exist "%ffmpeg_dir%" (
    echo Descargando ffmpeg...
    powershell -Command "(New-Object System.Net.WebClient).DownloadFile('%ffmpeg_url%', '%temp%\ffmpeg.zip')"
    powershell -Command Expand-Archive -Path "%temp%\ffmpeg.zip" -DestinationPath "%temp%"
)

:: Determinamos la carpeta de descargas del usuario
for /f "delims=" %%A in ('powershell -command "(New-Object -ComObject Shell.Application).NameSpace('shell:Downloads').Self.Path"') do set "download_folder=%%A\youtube"

:menu
:: Pedimos la direccion del video al usuario y dependiendo de lo que quiera el usuario se descargara la musica o el video
set /p "video=Introduce la URL del video de YouTube haciendo click derecho sobre esta ventana y dandole al enter: "

:: Comprobamos que ha introducido una url de youtube
if not "%video:~0,32%"=="https://www.youtube.com/watch?v=" (
    echo La direccion introducida no es correcta. Asegurese de que la URL sea correcta y comience por "https://www.youtube.com/watch?v="
    goto :menu
) else if "%video%" EQU "" (
    echo Tiene que introducir la direccion de un video
    goto :menu
) 

echo.
echo Seleccione una opcion:
echo [1] Descargar solo la musica
echo [2] Descargar el video
echo [3] Actualizar el programa
echo [4] Salir
echo.
set /p "opcion=Escriba la opcion deseada y pulse enter para continuar: "
echo.

:: Dependiendo de lo que quiera el usuario se descargara la musica o el video
if "%opcion%"=="1" (
    "%yt-dlp_dir%" -x --ffmpeg-location "%ffmpeg_dir%" ^
        --progress -q ^
        --extract-audio --audio-format mp3 ^
        -o "%download_folder%\%%(title)s.%%(ext)s" --restrict-filenames ^
        "%video%"
    echo La musica se ha descargado correctamente en "%download_folder%"
) else if "%opcion%"=="2" (
    "%yt-dlp_dir%" --ffmpeg-location "%ffmpeg_dir%" ^
        --progress -q ^
        -f bestvideo+bestaudio -S ext:mp4:m4a ^
        -o "%download_folder%\%%(title)s.%%(ext)s" --restrict-filenames ^
        "%video%"
    echo El video se ha descargado correctamente en "%download_folder%"
) else if "%opcion%"=="3" (
    powershell -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/Diego_LittleLion/getutube/-/raw/main/GetUTube.bat?inline=false', '%temp%/GetUTube.bat')"
    fc /b "GetUTube.bat" "%temp%\GetUTube.bat" > NUL
    if %errorlevel% NEQ 0 (
        echo Se ha encontrado una nueva version del programa. Actualizando...
        copy /y "%temp%\GetUTube.bat" "GetUTube.bat" > NUL
        echo Programa actualizado.
        pause
        exit
    )
) else if "%opcion%"=="4" (
    echo El programa se cerrara.
    pause
    exit
) else (
    echo Opcion invalida.
    echo.
)
echo ------------------------
goto :menu
